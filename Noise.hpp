#include "32blit.hpp"
#include <cstdlib>

using namespace std;

extern int maxValue;

class Noise{
	public:
		
		Noise(int max); // normally it will be 16 values
		Noise(int max, int amount);
		float getValue(int position);
		array<float, 16> getNoise();
};