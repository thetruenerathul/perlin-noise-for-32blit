file(REMOVE_RECURSE
  "CMakeFiles/BlitEngine.dir/engine/engine.cpp.o"
  "CMakeFiles/BlitEngine.dir/engine/geometry.cpp.o"
  "CMakeFiles/BlitEngine.dir/engine/input.cpp.o"
  "CMakeFiles/BlitEngine.dir/engine/output.cpp.o"
  "CMakeFiles/BlitEngine.dir/engine/particle.cpp.o"
  "CMakeFiles/BlitEngine.dir/engine/timer.cpp.o"
  "CMakeFiles/BlitEngine.dir/engine/tweening.cpp.o"
  "CMakeFiles/BlitEngine.dir/engine/utility.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/blend.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/color.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/filter.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/font.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/mask.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/mode7.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/primitive.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/sprite.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/surface.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/text.cpp.o"
  "CMakeFiles/BlitEngine.dir/graphics/tilemap.cpp.o"
  "CMakeFiles/BlitEngine.dir/math/interpolation.cpp.o"
  "CMakeFiles/BlitEngine.dir/types/map.cpp.o"
  "CMakeFiles/BlitEngine.dir/types/mat3.cpp.o"
  "CMakeFiles/BlitEngine.dir/types/mat4.cpp.o"
  "CMakeFiles/BlitEngine.dir/types/vec2.cpp.o"
  "CMakeFiles/BlitEngine.dir/types/vec3.cpp.o"
  "libBlitEngine.pdb"
  "libBlitEngine.a"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/BlitEngine.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
