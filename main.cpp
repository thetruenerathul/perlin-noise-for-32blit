#include "32blit.hpp"
#include "Noise.hpp"

using namespace std;
using namespace blit;

size screen_size(160, 120);

array<float, 16> noiseArray;

int a = 1;

void init() {
	Noise n(1);
	noiseArray = n.getNoise();

	screen_size.w = 160;
	screen_size.h = 120;
}

void render(uint32_t time) {
	fb.pen(rgba(255, 255, 255, 255));
	fb.clear();

	fb.pen(rgba(0, 0, 0, 255));
	for(int i = 0; i < noiseArray.size(); i++) {
		float y = 120.0/noiseArray[i];
		fb.pixel(point(10*i, 40 + y));
	}

	for(int i = 0; i < noiseArray.size() - 1; i++) {
		point p1(10*i, 40 + 120.0/noiseArray[i]);
		point p2(10*i + 10, 40 + 120.0/noiseArray[i + 1]);
		fb.line(p1, p2);
	}

	point p1(150, 40 + 120.0/noiseArray[15]);
	point p2(160, 40 + 120.0/noiseArray[0]);
	fb.line(p1, p2);
	int t = (int)(time/1000.0);
	if(a < t) {
		a++;
		Noise n(a);
		noiseArray = n.getNoise();
	}

}

void update(uint32_t time) {
	if(pressed(button(A))) {
		a++;
		Noise n(a);
		noiseArray = n.getNoise();
	}
}