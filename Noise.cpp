#include "32blit.hpp"
#include "Noise.hpp"

using namespace std;
using namespace blit;

int maxValue;
array<float, 16> noise;

void createNoise();

Noise::Noise(int max) {
	maxValue = max;
	createNoise();	
}

Noise::Noise(int max, int amount) {
	maxValue = max;
	createNoise();
}

float Noise::getValue(int position) {
	return noise[position];
}

array<float, 16> Noise::getNoise() {
	return noise;
}

void createNoise() {
	noise = array<float, 16>();
	array<float, 16> randoms = array<float, 16>();

	/* fill randoms with a random array of numbers with an offset for creation */
	for(int i = 0; i < 16; i++) {
		randoms[i] = (blit::random() % maxValue) + 5;
	}

	/* set groundlayer of random noise */
	for(int i = 0; i < 16; i++) {
		noise[i] = randoms[0];
	}

	/* add the noise factor over the array of noise over and over again to create a natural looking noise */
	for(int j = 8; j > 0; j = j/2) {
		for(int i = 0; i < 16; i=i+j){
			noise[i] = noise[i] + ((0.5 / (8/j)) * randoms[i]);
		}	
	}

	/* clean up the values according the noise interpolation */
	for(int i = 0; i < 16; i++) {
		noise[i] = noise[i] / 2.0;
	}
}